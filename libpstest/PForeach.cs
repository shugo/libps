﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders X be liable for any claim, damages or     *
 * other liability, whether in an action of contract, tort or     *
 * otherwise, arising from, out of or in connection with the      *
 * software or the use or other dealings in the Software.         *
 *                                                                *
 * Except as contained in this notice, the name of the            *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from the Raphael Boissel.  *
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace libpstest
{
    /// <summary>
    /// Summary description for PForeach
    /// </summary>
    [TestClass]
    public class PForeach
    {
        public PForeach()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestSmallList()
        {
            List<int> lst = new List<int>();
            for (int n = 0; n < 6; n++)
            { lst.Add(n); }
            libps.Iterators.Foreach.ParallelForeach(lst, (int value) => { if (value % 2 == 0) { return 0; } return 1; });
            for (int n = 0; n < lst.Count; n++)
            {
                Assert.AreEqual(n % 2, lst[n] % 2);
            }
        }

        [TestMethod]
        public void TestMediumList()
        {
            List<int> lst = new List<int>();
            for (int n = 0; n < 601; n++)
            { lst.Add(n); }
            libps.Iterators.Foreach.ParallelForeach(lst, (int value) => { if (value % 2 == 0) { return 0; } return 1; });
            for (int n = 0; n < lst.Count; n++)
            {
                Assert.AreEqual(n % 2, lst[n] % 2);
            }
        }

        [TestMethod]
        public void TestBigList()
        {
            List<int> lst = new List<int>();
            for (int n = 0; n < 6001; n++)
            { lst.Add(n); }
            libps.Iterators.Foreach.ParallelForeach(lst, (int value) => { if (value % 2 == 0) { return 0; } return 1; });
            for (int n = 0; n < lst.Count; n++)
            {
                Assert.AreEqual(n % 2, lst[n] % 2);
            }
        }
    }
}
