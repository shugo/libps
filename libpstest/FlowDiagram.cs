﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders X be liable for any claim, damages or     *
 * other liability, whether in an action of contract, tort or     *
 * otherwise, arising from, out of or in connection with the      *
 * software or the use or other dealings in the Software.         *
 *                                                                *
 * Except as contained in this notice, the name of the            *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from the Raphael Boissel.  *
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace libpstest
{
    /// <summary>
    /// Summary description for FlowDiagram
    /// </summary>
    [TestClass]
    public class FlowDiagram
    {
        public FlowDiagram()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void CreateDiagram()
        {
            libps.Structures.FlowDiagram Diagram = new libps.Structures.FlowDiagram();
            libps.Structures.FlowDiagram.Node<int> Filter1 =
                new libps.Structures.FlowDiagram.Node<int>
                    (
                        (object[] obj) =>
                        {
                            int a = (int)obj[0];
                            int b = (int)obj[1];
                            return a + b;
                        }
                    );
            libps.Structures.FlowDiagram.Node<int> Filter2 =
                new libps.Structures.FlowDiagram.Node<int>
                    (
                        (object[] obj) =>
                        {
                            int a = (int)obj[0];
                            int b = (int)obj[1];
                            return a * b;
                        }
                    );
            libps.Structures.FlowDiagram.Source<int> Cst1 =
                new libps.Structures.FlowDiagram.Source<int>(() => { return 1; });
            libps.Structures.FlowDiagram.Source<int> Cst2 =
                new libps.Structures.FlowDiagram.Source<int>(() => { return 4; });
            Filter1.CreateInputPort<int>().Bind(Cst1.OutputPort);
            Filter1.CreateInputPort<int>().Bind(Cst1.OutputPort);
            Filter2.CreateInputPort<int>().Bind(Filter1.OutputPort);
            Filter2.CreateInputPort<int>().Bind(Cst2.OutputPort);
            libps.Structures.FlowDiagram.Sink<int> Output1 = new libps.Structures.FlowDiagram.Sink<int>
                ((int value) => { Assert.AreEqual(8, value); Diagram.Stop(); });
            Output1.InputPort.Bind(Filter2.OutputPort);
            Diagram.Add<int>(Output1);
            Diagram.Start();
            Diagram.Wait();
        }

        [TestMethod]
        public void MultipleSinkDiagram()
        {
            libps.Structures.FlowDiagram Diagram = new libps.Structures.FlowDiagram();
            libps.Structures.FlowDiagram.Node<int> Filter1 =
                new libps.Structures.FlowDiagram.Node<int>
                    (
                        (object[] obj) =>
                        {
                            int a = (int)obj[0];
                            int b = (int)obj[1];
                            return a + b;
                        }
                    );
            libps.Structures.FlowDiagram.Node<int> Filter2 =
                new libps.Structures.FlowDiagram.Node<int>
                    (
                        (object[] obj) =>
                        {
                            int a = (int)obj[0];
                            int b = (int)obj[1];
                            return a * b;
                        }
                    );
            libps.Structures.FlowDiagram.Source<int> Cst1 =
                new libps.Structures.FlowDiagram.Source<int>(() => { return 1; });
            libps.Structures.FlowDiagram.Source<int> Cst2 =
                new libps.Structures.FlowDiagram.Source<int>(() => { return 4; });
            Filter1.CreateInputPort<int>().Bind(Cst1.OutputPort);
            Filter1.CreateInputPort<int>().Bind(Cst1.OutputPort);
            Filter2.CreateInputPort<int>().Bind(Filter1.OutputPort);
            Filter2.CreateInputPort<int>().Bind(Cst2.OutputPort);
            bool set = false;
            libps.Structures.FlowDiagram.Sink<int> Output1 = new libps.Structures.FlowDiagram.Sink<int>
                ((int value) => { Assert.AreEqual(8, value); if (set) { Diagram.Stop(); } });
            libps.Structures.FlowDiagram.Sink<int> Output2 = new libps.Structures.FlowDiagram.Sink<int>
                ((int value) => { Assert.AreEqual(4, value); set = true; });
            Output1.InputPort.Bind(Filter2.OutputPort);
            Output2.InputPort.Bind(Cst2.OutputPort);
            Diagram.Add<int>(Output1);
            Diagram.Add<int>(Output2);
            Diagram.Start();
            Diagram.Wait();
        }
        [TestMethod]
        public void OrderedDiagram()
        {
            int emitDiag = 1;
            int expected = 1;
            libps.Structures.FlowDiagram Diagram = new libps.Structures.FlowDiagram();
            libps.Structures.FlowDiagram.Node<int> Filter1 =
                new libps.Structures.FlowDiagram.Node<int>
                    (
                        (object[] obj) =>
                        {
                            int a = (int)obj[0];
                            int b = (int)obj[1];
                            return a + b;
                        }
                    );
            libps.Structures.FlowDiagram.Node<int> Filter2 =
                new libps.Structures.FlowDiagram.Node<int>
                    (
                        (object[] obj) =>
                        {
                            lock (this)
                            {
                                int a = (int)obj[0];
                                int b = (int)obj[1];
                                return a * b;
                            }
                        }
                    );
            emitDiag = 1;
            libps.Structures.FlowDiagram.Source<int> Cst1 =
                new libps.Structures.FlowDiagram.Source<int>(() => { lock (this) { return emitDiag++; } });
            libps.Structures.FlowDiagram.Source<int> Cst2 =
                new libps.Structures.FlowDiagram.Source<int>(() => { return 4; });
            Filter1.CreateInputPort<int>().Bind(Cst1.OutputPort);
            Filter1.CreateInputPort<int>().Bind(Cst1.OutputPort);
            Filter2.CreateInputPort<int>().Bind(Filter1.OutputPort);
            Filter2.CreateInputPort<int>().Bind(Cst2.OutputPort);
            expected = 1;
            libps.Structures.FlowDiagram.Sink<int> Output1 = new libps.Structures.FlowDiagram.Sink<int>
                ((int value) => { lock (this) { Assert.AreEqual(expected++ * 8, value); if (expected == 0xFFFF) { Diagram.Stop(); } } });
            Output1.InputPort.Bind(Filter2.OutputPort);
            Diagram.Add<int>(Output1);
            Diagram.Start();
            Diagram.Wait();
        }
    }
}
