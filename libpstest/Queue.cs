﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders X be liable for any claim, damages or     *
 * other liability, whether in an action of contract, tort or     *
 * otherwise, arising from, out of or in connection with the      *
 * software or the use or other dealings in the Software.         *
 *                                                                *
 * Except as contained in this notice, the name of the            *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from the Raphael Boissel.  *
 */

using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace libpstest
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class Queue
    {
        public Queue()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestSyncQueue()
        {
            libps.Collections.Queue<int> queue = new libps.Collections.Queue<int>();
            for (int n = 0; n < 10000; n++) { queue.Enqueue(n); }
            for (int n = 0; n < 10000; n++) { Assert.AreEqual<int>(queue.Dequeue(), n); }
        }

        [TestMethod]
        public void TestAsyncEnqueueQueue()
        {
            libps.Collections.Queue<int> queue = new libps.Collections.Queue<int>();
            List<int> numbers = new List<int>();
            for (int n = 0; n < 10000; n++) { numbers.Add(1); }
            libps.Iterators.Foreach.ParallelForeach(numbers, (int number) => { queue.Enqueue(number); return number; });
            queue.Enqueue(0);
            for (int n = 0; n < 10000; n++) { Assert.AreEqual<int>(1, queue.Dequeue()); }
            Assert.AreEqual<int>(queue.Dequeue(), 0);
        }

        [TestMethod]
        public void TestAsyncDequeueQueue()
        {
            libps.Collections.Queue<int> queue = new libps.Collections.Queue<int>();
            List<int> numbers = new List<int>();
            for (int n = 0; n < 10000; n++) { numbers.Add(1); queue.Enqueue(42); }
            libps.Iterators.Foreach.ParallelForeach(numbers, (int number) => { return queue.Dequeue();});
            Assert.AreEqual<int>(queue.Dequeue(), 0);
            for (int n = 0; n < 10000; n++) { Assert.AreEqual<int>(42, numbers[n]); }
        }

        [TestMethod]
        public void TestSyncEmptyQueue()
        {
            libps.Collections.Queue<int> queue = new libps.Collections.Queue<int>();
            for (int n = 0; n < 10000; n++)
            {
                Assert.AreEqual<int>(0, queue.Dequeue());
                queue.Enqueue(n);
                Assert.AreEqual<bool>(false, queue.IsEmpty());
                Assert.AreEqual<int>(n, queue.Dequeue());
                Assert.AreEqual<int>(0, queue.Dequeue());
                Assert.AreEqual<bool>(true, queue.IsEmpty());
            }
        }

        [TestMethod]
        public void TestEnqueueDequeueQueue_StressTest()
        {
            libps.Collections.Queue<int> queue = new libps.Collections.Queue<int>();
            System.Threading.Thread A = new System.Threading.Thread(
                () => { for (int n = 0; n < 100000; n++) { queue.Enqueue(1); } });
            System.Threading.Thread B = new System.Threading.Thread(
                () => { for (int n = 0; n < 100000; n++) { queue.Enqueue(1); } });

            System.Threading.Thread C = new System.Threading.Thread(
                () => 
                {
                    int cb = 200000;
                    while (cb > 0)
                    {
                        if (queue.Dequeue() != 0)
                        {
                            cb--;
                        }
                    }
                });
            System.Threading.Thread D = new System.Threading.Thread(
                () =>
                {
                    for (int n = 0; n < 100000; n++) { queue.Enqueue(1); while (queue.Dequeue() == 0) { }; } 
                });
            A.Start();
            B.Start();
            C.Start();
            A.Join();
            B.Join();
            Assert.IsTrue(C.Join(40000));
        }
    }
}
