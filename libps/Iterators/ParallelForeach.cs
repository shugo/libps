﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders X be liable for any claim, damages or     *
 * other liability, whether in an action of contract, tort or     *
 * otherwise, arising from, out of or in connection with the      *
 * software or the use or other dealings in the Software.         *
 *                                                                *
 * Except as contained in this notice, the name of the            *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from the Raphael Boissel.  *
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Iterators
{
    static public class Foreach
    {
        public delegate Type Method<Type>(Type input);
        public delegate void Procedure<Type>(Type input);
        public delegate void Task();
        class InternalClosureList<Type>
        {
            List<Type> list;
            int start = 0;
            int group = 0;
            Method<Type> method;
            public InternalClosureList(List<Type> List, int Start, int Group, Method<Type> Method)
            {
                list = List;
                start = Start;
                method = Method;
                group = Group;
            }
            public void run()
            {
                ParallelForeach<Type>(list, method, start, group);
            }
        }

        class InternalClosureArray<Type>
        {
            Type[] array;
            int start = 0;
            int group = 0;
            Method<Type> method;
            public InternalClosureArray(Type[] Array, int Start, int Group, Method<Type> Method)
            {
                array = Array;
                start = Start;
                method = Method;
                group = Group;
            }
            public void run()
            {
                ParallelForeach<Type>(array, method, start, group);
            }
        }

        public static void ParallelForeach<Type>(List<Type> list, Method<Type> method)
        {
           ParallelForeach<Type>(list, method, System.Environment.ProcessorCount);
        }

        public static void ParallelForeach<Type>(List<Type> list, Method<Type> method, int nbThread)
        {
            if (nbThread <= 0) { nbThread = 1; }
            int count = list.Count / nbThread;
            if (count < 1) { count = 1; }
            ParallelForeach<Type>(list, method, 0, count);
        }

        internal static void ParallelForeach<Type>(List<Type> list, Method<Type> method, int offset, int group)
        {
            if (list.Count - offset < group)
            {
                unchecked
                {
                    int n = offset;
                    int count = list.Count;
                    int total = count;
                    count -= count % 4;
                    for (; n < count; n++)
                    {
                        list[n] = method(list[n]); n++;
                        list[n] = method(list[n]); n++;
                        list[n] = method(list[n]); n++;
                        list[n] = method(list[n]);
                    }
                    for (; n < total; n++)
                    {
                        list[n] = method(list[n]);
                    }
                }
                return;
            }
            InternalClosureList<Type> closure = new InternalClosureList<Type>(list, offset + group, group, method);
            System.Threading.Thread process = new System.Threading.Thread(closure.run);
            process.IsBackground = true;
            process.Start();
            unchecked 
            {
                int n = offset;
                int end = offset + group;
                int totalend = end;
                end -= end % 4;
                end -= 4;
                for (; n < end; n++)
                {
                    list[n] = method(list[n]); n++;
                    list[n] = method(list[n]); n++;
                    list[n] = method(list[n]); n++;
                    list[n] = method(list[n]); 
                }
                for (; n < totalend; n++)
                { list[n] = method(list[n]); }
            }
            process.Join();
        }

        public static void ParallelForeach<Type>(Type[] array, Method<Type> method)
        {
            ParallelForeach<Type>(array, method, System.Environment.ProcessorCount);
        }

        public static void ParallelForeach<Type>(Type[] array, Method<Type> method, int nbThread)
        {
            if (nbThread <= 0) { nbThread = 1; }
            int count = array.Length / nbThread;
            if (count < 1) { count = 1; }
            ParallelForeach<Type>(array, method, 0, count);
        }

        internal static void ParallelForeach<Type>(Type[] array, Method<Type> method, int offset, int group)
        {
            if (array.Length - offset < group)
            {
                unchecked
                {
                    int n = offset;
                    int length = array.Length;
                    int total = length;
                    length -= length % 4;
                    for (; n < length; n++)
                    {
                        array[n] = method(array[n]); n++;
                        array[n] = method(array[n]); n++;
                        array[n] = method(array[n]); n++;
                        array[n] = method(array[n]);
                    }
                    for (; n < total; n++)
                    {
                        array[n] = method(array[n]);
                    }
                }
                return;
            }
            InternalClosureArray<Type> closure = new InternalClosureArray<Type>(array, offset + group, group, method);
            System.Threading.Thread process = new System.Threading.Thread(closure.run);
            process.IsBackground = true;
            process.Start();
            unchecked
            {
                int n = offset;
                int end = offset + group;
                int totalend = end;
                end -= totalend % 4;
                for (; n < end; n++)
                {
                    array[n] = method(array[n]); n++;
                    array[n] = method(array[n]); n++;
                    array[n] = method(array[n]); n++;
                    array[n] = method(array[n]);
                }
                for (; n < totalend; n++)
                {
                    array[n] = method(array[n]);
                }
            }
            process.Join();
        }
    }
}
