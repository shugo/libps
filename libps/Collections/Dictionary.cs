﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders X be liable for any claim, damages or     *
 * other liability, whether in an action of contract, tort or     *
 * otherwise, arising from, out of or in connection with the      *
 * software or the use or other dealings in the Software.         *
 *                                                                *
 * Except as contained in this notice, the name of                *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from Raphael Boissel.      *
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Collections
{
    public class Dictionary<Key, Value>
    {
         System.Collections.Generic.Dictionary<Key, Value> _behind;
        Locks.RWLock _lock = new Locks.RWLock();
        /// <summary>
        /// Create a new Dictionary
        /// </summary>
        public Dictionary()
        {
            _behind = new System.Collections.Generic.Dictionary<Key, Value>();
        }
        /// <summary>
        /// Add an element to the dictionary.
        /// </summary>
        /// <param name="item">The element that will be added</param>
        public void Add(Key Key, Value Value)
        {
            _lock.WriteLock();
            _behind.Add(Key, Value);
            _lock.WriteUnlock();
        }
        /// <summary>
        /// Remove an element from the dictionary.
        /// </summary>
        /// <param name="item">The element that must be removed</param>
        public void Remove(Key Key)
        {
            _lock.WriteLock();
            _behind.Remove(Key);
            _lock.WriteUnlock();
        }

        /// <summary>
        ///Clear the dictionary.
        /// </summary>
        public void Clear()
        {
            _lock.WriteLock();
            _behind.Clear();
            _lock.WriteUnlock();
        }

        /// <summary>
        /// Try to get an element from the dictionary
        /// </summary>
        public bool TryGetValue(Key Key, out Value Value)
        {
            _lock.ReadLock();
            bool result = _behind.TryGetValue(Key, out Value);
            _lock.ReadUnlock();
            return result;
        }
        /// <summary>
        /// Get the number of element in the list.
        /// </summary>
        public int Count
        {
            get { _lock.ReadLock(); int count = _behind.Count; _lock.ReadUnlock(); return count; }
        }

        /// <summary>
        /// Get or set an element.
        /// </summary>
        /// <remarks>You can't set an element inside a foreach loop.</remarks>
        /// <param name="index">The index of the element</param>
        /// <returns></returns>
        public Value this[Key index]
        {
            get { _lock.ReadLock(); Value value = _behind[index]; _lock.ReadUnlock(); return value; }
            set { _lock.WriteLock(); _behind[index] = value; _lock.WriteUnlock(); }
        }
    }
}
