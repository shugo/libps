﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders Raphael Boissel be liable for any claim,  *
 * damages or other liability, whether in an action of contract,  *
 * tort or otherwise, arising from, out of or in connection with  *
 * the software or the use or other dealings in the Software.     *
 *                                                                *
 * Except as contained in this notice, the name of                *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from Raphael Boissel.      *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Collections
{
    /// <summary>
    /// Lock-Free Queue
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Queue<T>
    {
        class Link
        {
            internal T Item;
            internal bool End;
            public Link() { Item = default(T); End = true; }
            public Link(T Item) { this.Item = Item; End = false;}
            internal Link Next = null;
        }

        Link _Head = new Link();
        Link _Tail;
        /// <summary>
        /// Create a new Lock-Free queue
        /// </summary>
        public Queue()
        {
            _Tail = _Head;
        }
        /// <summary>
        /// Test if the list is empty.
        /// </summary>
        /// <returns>true if the list is empty false otherwise</returns>
        public bool IsEmpty()
        {
            return _Tail.End;
        }

        /// <summary>
        /// Enqueue a new item in the queue.
        /// </summary>
        /// <param name="Item"></param>
        public void Enqueue(T Item)
        {
            Link ln = new Queue<T>.Link();
            Link oldHead; 
            do
            {
                oldHead = _Head;
                Link next = oldHead.Next;
                if (_Head == oldHead)
                {
                    if (next != null)
                    {
                        System.Threading.Interlocked.CompareExchange<Link>(ref _Head, next, oldHead);
                    }
                    else
                    {
                        Link before = (System.Threading.Interlocked.CompareExchange<Link>(ref oldHead.Next, ln, null));
                        if (before == null)
                        {
                            System.Threading.Interlocked.CompareExchange<Link>(ref _Head, ln, oldHead);
                            oldHead.Item = Item;
                            oldHead.End = false;
                            return;
                        }
                    }
                }
            }  while (true);
        }

        /// <summary>
        /// Dequeue an item.
        /// </summary>
        /// <returns></returns>
        public T Dequeue()
        {
            Link OldTail;
            do
            {
                OldTail = _Tail;
                Link next = OldTail.Next;
                if (OldTail.End || next == null) { return default(T); }
                if (_Tail == OldTail)
                {
                        Link Before = System.Threading.Interlocked.CompareExchange<Link>(ref _Tail, next, OldTail);
                        if (Before == OldTail)
                        {
                            return Before.Item;
                        }
                }
            } while (true);
        }
    }
}
