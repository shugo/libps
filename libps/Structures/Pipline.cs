﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders Raphael Boissel be liable for any claim,  *
 * damages or other liability, whether in an action of contract,  *
 * tort or otherwise, arising from, out of or in connection with  *
 * the software or the use or other dealings in the Software.     *
 *                                                                *
 * Except as contained in this notice, the name of                *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from Raphael Boissel.      *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Structures
{
    /// <summary>
    /// Pipline can be used to process a data using multiple filters.
    /// This implementation is Lock-Free.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Pipline<T>
    {
        public delegate T InMethod();
        public delegate void OutMethod(T obj);
        public delegate T Method(T obj);
        class TokenObject
        {
            volatile internal UInt32 _uid = 0;
            internal T _object = default(T);
        }
        class Stage
        {
            Method _method = null;
            internal Stage _nextStage = null;
            internal Collections.Queue<TokenObject> _queue = new libps.Collections.Queue<TokenObject>();
            Collections.Queue<TokenObject> _outQueue = null;
            public Stage(Method Method, Collections.Queue<TokenObject> OutQueue) { _method = Method; _outQueue = OutQueue; }
            public void RunOnce()
            {
                TokenObject obj = _queue.Dequeue();
                if (obj != null)
                {
                    obj._object = _method(obj._object);
                    if (_nextStage != null)
                    {
                        _nextStage._queue.Enqueue(obj);
                    }
                    else
                    {
                        _outQueue.Enqueue(obj);
                    }
                }
                else
                {
                    // Yield to the scheduler
                    System.Threading.Thread.Sleep(0);
                }
            }
        }

        volatile int _nbToken = 0;
        volatile bool _running = false;
        int _nbMaxToken = 0;
        int _nbThread = 0;
        List<System.Threading.Thread> _threads = new List<System.Threading.Thread>();
        Stage[] _stages = null;
        Collections.Queue<TokenObject> _outQueue = new libps.Collections.Queue<TokenObject>();
        InMethod _inMethod = null;
        OutMethod _outMethod = null;

        /// <summary>
        /// Create a new pipline
        /// </summary>
        /// <param name="nbToken">Number of element that can be
        /// in the pipline at the same time</param>
        /// <param name="In">The method of the reader</param>
        /// <param name="Out">The method of the writer</param>
        /// <param name="Stages">The methods of the filters</param>
        public Pipline(int nbToken, InMethod In, OutMethod Out, params Method[] Stages) :
            this(nbToken, System.Environment.ProcessorCount, In, Out, Stages)
        {
        }

        /// <summary>
        /// Create a new pipline
        /// </summary>
        /// <param name="nbToken">Number of element that can be
        /// in the pipline at the same time</param>
        /// <param name="nbThread">Number of thread used by the pipline</param>
        /// <param name="In">The method of the reader</param>
        /// <param name="Out">The method of the writer</param>
        /// <param name="Stages">The methods of the filters</param>
        public Pipline(int nbToken, int nbThread, InMethod In, OutMethod Out, params Method[] Stages) 
        {
            _nbMaxToken = nbToken;
            _nbToken = 0;
            _nbThread = nbThread;
            _inMethod = In;
            _outMethod = Out;
            _stages = new Stage[Stages.Length];
            for (int n = 0; n < Stages.Length; n++)
            {
                _stages[n] = new Pipline<T>.Stage(Stages[n], _outQueue);
                if (n != 0) { _stages[n - 1]._nextStage = _stages[n]; }
            }
        }

        /// <summary>
        /// Start the pipline. This is a non-blocking call.
        /// </summary>
        public void Start()
        {
            _running = true;
            if (_nbThread >= 2) 
            {
                for (int n = 0; n < _nbThread - 2; n++)
                {
                    _threads.Add(new System.Threading.Thread(_stdLoop));
                }
                _threads.Add(new System.Threading.Thread(_inLoop));
                _threads.Add(new System.Threading.Thread(_outLoop));
            }
            foreach (System.Threading.Thread thread in _threads)
            { thread.Start(); }
        }

        /// <summary>
        /// Stop the pipline. This is a non-blocking call.
        /// </summary>
        public void Stop()
        {
            _running = false;
        }

        public bool Working
        {
            get 
            {
                if (_running)
                    return true;
                foreach (System.Threading.Thread thread in _threads)
                { if (thread.ThreadState == System.Threading.ThreadState.Running) { return true; } }
                return false;
            }
        }

        /// <summary>
        /// Wait while all the thread in the pipline have
        /// ended. This is a blocking call.
        /// </summary>
        public void Wait()
        {
            foreach (System.Threading.Thread thread in _threads)
                thread.Join();
        }

        void _outLoop()
        {
            unchecked
            {
                UInt32 uid = 0;
                while (_running)
                {
                    TokenObject obj = _outQueue.Dequeue();
                    if (obj != null)
                    {
                        if (obj._uid == uid)
                        {
                            System.Threading.Interlocked.Decrement(ref _nbToken);
                            uid++;
                            _outMethod(obj._object);
                        }
                        else
                        {
                            _outQueue.Enqueue(obj);
                        }
                    }
                    else
                    {
                        int length = _stages.Length;
                        for (int n = 0; n < length; n++) { _stages[n].RunOnce(); }
                    }
                }
            }
        }

        void _stdLoop()
        {
            int length = _stages.Length;
            while (_running)
            {
                for(int n = 0; n < length; n++) { _stages[n].RunOnce(); }
            }
        }

        void _inLoop()
        {
            unchecked
            {
                UInt32 uid = 0;
                while (_running)
                {
                    if (_nbToken < _nbMaxToken)
                    {
                        TokenObject obj = new Pipline<T>.TokenObject();
                        obj._object = _inMethod();
                        obj._uid = uid;
                        System.Threading.Interlocked.Increment(ref _nbToken);
                        _stages[0]._queue.Enqueue(obj);
                        uid++;
                    }
                    else 
                    {
                        int length = _stages.Length;
                        for (int n = 0; n < length; n++) { _stages[n].RunOnce(); }
                    }
                }
            }
        }
    }
}
