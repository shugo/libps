﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders Raphael Boissel be liable for any claim,  *
 * damages or other liability, whether in an action of contract,  *
 * tort or otherwise, arising from, out of or in connection with  *
 * the software or the use or other dealings in the Software.     *
 *                                                                *
 * Except as contained in this notice, the name of                *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from Raphael Boissel.      *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Structures
{
    /// <summary>
    /// A parallel flow diagram.
    /// </summary>
    public class FlowDiagram
    {
        /// <summary>
        /// Represent a sink of a flow diagram.
        /// </summary>
        /// <typeparam name="Type"></typeparam>
        public class Sink<Type>
        {
            public delegate void Action(Type Object);
            Action _Action;

            InputPort<Type> _inputPort = new InputPort<Type>();

            public Sink(Action Action)
            {
                _Action = Action;
            }

            public InputPort<Type> InputPort
            {
                get { return _inputPort; }
            }
            volatile int _expected = 1;
            class PendingRequest
            {
                int _id;

                public int Id
                {
                    get { return _id; }
                    set { _id = value; }
                }
                Type _value;

                public Type Value
                {
                    get { return _value; }
                    set { _value = value; }
                }

            }
            List<PendingRequest> _pendingRequest = new List<PendingRequest>();
            internal void Request(int Id)
            {
                Type result = (Type)_inputPort.Request(Id);
                lock (this)
                {
                    if (Id == _expected)
                    {
                        _Action(result);
                        _expected++;
                        bool has = true;
                        do
                        {
                            has = false;
                            foreach (PendingRequest request in _pendingRequest)
                            {
                                if (request.Id == _expected)
                                {
                                    _Action(request.Value);
                                    _pendingRequest.Remove(request);
                                    _expected++;
                                    has = true;
                                    break;
                                }
                            }
                        } while (has);
                    }
                    else
                    {
                        PendingRequest PendingRequest = new Sink<Type>.PendingRequest();
                        PendingRequest.Id = Id;
                        PendingRequest.Value = result;
                        _pendingRequest.Add(PendingRequest);
                    }
                }
            }
            internal void FlushCache(int Id)
            {
                _inputPort.FlushCache(Id);
            }
        }
        class SinkAdaptator
        {
            internal SinkAdaptator()
            {
            }
            delegate void RequestMethod(int Id);
            RequestMethod _RequestMethod = null;
            delegate void FlushCacheMethod(int Id);
            FlushCacheMethod _FlushCacheMethod = null;
            internal void Bind<Type>(Sink<Type> Sink)
            {
                _RequestMethod = Sink.Request;
                _FlushCacheMethod = Sink.FlushCache;
            }

            internal void Request(int Id)
            {
                _RequestMethod(Id);
            }

            internal void FlushCache(int Id)
            {
                _FlushCacheMethod(Id);
            }
        }
        public class Source<Type>
        {
            OutputPort<Type> _outputPort;

            public OutputPort<Type> OutputPort
            {
                get { return _outputPort; }
            }
            public delegate Type Action();
            Action _sourceAction;
            public Source(Action Action)
                : base()
            {
                _sourceAction = Action;
                _outputPort = new OutputPort<Type>(Request, FlushCache);
            }
            volatile int _expected = 1;
            Dictionary<int, object> _mem = new Dictionary<int, object>();

            internal object Request(int Id)
            {
                lock (this)
                {
                    while (Id >= _expected)
                    {
                        _mem.Add(_expected, _sourceAction());
                        _expected++;
                    }
                    return _mem[Id];
                }
            }

            internal void FlushCache(int Id)
            {
                lock (this)
                {
                    _mem.Remove(Id);
                }
            }
        }

        /// <summary>
        /// Represent a generic node.
        /// </summary>
        /// <typeparam name="OutputType"></typeparam>
        public class Node<OutputType>
        {
            OutputPort<OutputType> _outputPort;
            public delegate OutputType Action(params object[] Inputs);
            internal protected Action _action = null;
            internal Node()
            {
                _outputPort = new OutputPort<OutputType>(Request, FlushCache);
            }
            public Node(Action Action)
            {
                _outputPort = new OutputPort<OutputType>(Request, FlushCache);
                _action = Action;
            }

            public OutputPort<OutputType> OutputPort
            {
                get { return _outputPort; }
            }

            delegate object GetInput(int id);
            Collections.List<GetInput> _inputs = new libps.Collections.List<GetInput>();
            delegate void FlushInput(int id);
            Collections.List<FlushInput> _flushinputs = new libps.Collections.List<FlushInput>();
            public InputPort<Type> CreateInputPort<Type>()
            {
                InputPort<Type> inputPort = new InputPort<Type>();
                _inputs.Add(inputPort.Request);
                _flushinputs.Add(inputPort.FlushCache);
                return inputPort;
            }

            Dictionary<int, object> _mem = new Dictionary<int, object>();
            volatile bool _minmaxSet = false;
            volatile int _minId = 0;
            volatile int _maxId = 0;
            internal void FlushCache(int Id)
            {
                lock (this)
                {
                    _mem.Remove(Id);
                    foreach (FlushInput flush in _flushinputs)
                    {
                        flush(Id);
                    }
                }
            }
            internal object Request(int Id)
            {
                lock (this)
                {
                    object result = null;
                    if (_mem.TryGetValue(Id, out result))
                        return result;
                    List<object> _object = new List<object>();
                    foreach (GetInput input in _inputs)
                    {
                        _object.Add(input(Id));
                    }
                    result = _action(_object.ToArray());
                    _mem.Add(Id, result);
                    return result;
                }
            }
        }
        /// <summary>
        /// Represent the output of a node. Each node in a flow
        /// diagram has an input except the sink.
        /// </summary>
        /// <typeparam name="Type"></typeparam>
        public class OutputPort<Type>
        {
            internal delegate object RequestMethod(int id);
            RequestMethod _requestMethod = null;
            internal delegate void FlushMethod(int id);
            FlushMethod _flushMethod = null;
            internal OutputPort(RequestMethod RequestMethod, FlushMethod FlushMethod)
            {
                _requestMethod = RequestMethod;
                _flushMethod = FlushMethod;
            }

            internal Collections.List<InputPort<Type>> _binded = new libps.Collections.List<InputPort<Type>>();
            public void Bind(InputPort<Type> Port)
            {
                _binded.Add(Port);
                if (Port._binded == null)
                {
                    Port._binded = this;
                }
                else
                {
                    throw new Exception("An input port can be binded to only one output port");
                }
            }
            internal object Request(int id)
            {
                return _requestMethod(id);
            }
            internal void FlushCache(int id)
            {
                _flushMethod(id);
            }
        }
        /// <summary>
        /// Represent an input of a node. Each node
        /// in a flow diagram has inputs execpt the source node.
        /// </summary>
        /// <typeparam name="Type"></typeparam>
        public class InputPort<Type>
        {
            internal OutputPort<Type> _binded = null;
            public void Bind(OutputPort<Type> Port)
            {
                if (_binded == null)
                {
                    _binded = Port;
                    Port._binded.Add(this);
                }
                else
                {
                    throw new Exception("An input port can be binded to only one output port");
                }

            }
            internal object Request(int id)
            {
                if (_binded == null) { return null; }
                return _binded.Request(id);
            }
            internal void FlushCache(int id)
            {
                if (_binded != null) { _binded.FlushCache(id); }
            }
        }

        /// <summary>
        /// Internal class that store a request for the flow diagram
        /// </summary>
        class RequestId
        {
            internal class RefCount
            {
                int _value = 0;
                public RefCount(int Cb)
                {
                    _value = Cb;
                }
                public bool Release()
                {
                    return (System.Threading.Interlocked.Decrement(ref _value) <= 0);            
                }
            }
            SinkAdaptator _sink = null;

            public SinkAdaptator Sink
            {
                get { return _sink; }
                set { _sink = value; }
            }

            Collections.List<SinkAdaptator> _sinks = null;

            public Collections.List<SinkAdaptator> Sinks
            {
                get { return _sinks; }
                set { _sinks = value; }
            }

            public void TryFlushCache()
            {
                if (_refcount.Release())
                {
                    foreach (SinkAdaptator sink in _sinks)
                    {
                        sink.FlushCache(_id);
                    }
                }
            }


            int _id = 0;

            public int Id
            {
                get { return _id; }
                set { _id = value; }
            }

            private RefCount _refcount = null;

            internal RefCount Refcount
            {
                get { return _refcount; }
                set { _refcount = value; }
            }
 
        }

        Collections.List<SinkAdaptator> _sinks = new libps.Collections.List<SinkAdaptator>();
        public void Add<Type>(Sink<Type> Sink)
        {
            SinkAdaptator adaptator = new SinkAdaptator();
            adaptator.Bind(Sink);
            _sinks.Add(adaptator);
        }

        Collections.Queue<RequestId> _pendingRequests = new Collections.Queue<RequestId>();
        int _currentId = 0;
        bool _running = false;
        List<System.Threading.Thread> _threads = new List<System.Threading.Thread>();
        int _nbThread = 1;
        public FlowDiagram()
            : this(System.Environment.ProcessorCount)
        {
        }
        public FlowDiagram(int nbThread)
        {
            _nbThread = nbThread;
            if (_nbThread > System.Environment.ProcessorCount)
                _nbThread = System.Environment.ProcessorCount;
        }
        /// <summary>
        /// Start the flow diagram. This is a non-blocking call.
        /// </summary>
        public void Start()
        {
            _running = true;
            for (int n = 0; n < _nbThread; n++)
            {
                _threads.Add(new System.Threading.Thread(_loop));
            }
            foreach (System.Threading.Thread thread in _threads)
            { thread.Start(); }
        }

        /// <summary>
        /// Stop the flow diagram. This is a non-blocking call.
        /// </summary>
        public void Stop()
        {
            _running = false;
        }

        /// <summary>
        /// Wait while all the thread in the flow diagram have
        /// ended. This is a blocking call.
        /// </summary>
        public void Wait()
        {
            foreach (System.Threading.Thread thread in _threads)
                thread.Join();
        }

        void _loop()
        {
            while (_running)
            {
                if (_pendingRequests.IsEmpty())
                {
                        int id = System.Threading.Interlocked.Increment(ref _currentId);
                        bool isfirst = true;
                        SinkAdaptator first = null;
                        // Do not move the refcount initialization
                        // adn the sink list initialization
                        // outside the foreach. Indead the list must not be unlocked
                        // during the initilization.
                        RequestId.RefCount refcount = null;
                        Collections.List<SinkAdaptator> sinkscpy = null;
                        foreach (SinkAdaptator sink in _sinks)
                        {
                            if (refcount == null)
                            {
                                refcount = new RequestId.RefCount(_sinks.Count);
                            }
                            if (sinkscpy == null)
                            {
                                sinkscpy = _sinks.Clone();
                            }
                            if (isfirst)
                            {
                                first = sink; isfirst = false;
                            }
                            else
                            {
                                RequestId request = new RequestId();
                                request.Refcount = refcount;
                                request.Id = id;
                                request.Sink = sink;
                                request.Sinks = sinkscpy;
                                _pendingRequests.Enqueue(request);
                            }
                        }
                        if (first != null)
                        {
                            first.Request(id);
                            if (refcount.Release() && sinkscpy != null)
                            {
                                foreach (SinkAdaptator sink in sinkscpy)
                                {
                                    sink.FlushCache(id);
                                }
                            }
                        }
                }
                else
                {
                    RequestId request = _pendingRequests.Dequeue();
                    if (request != null)
                    {
                        if (request.Sink != null)
                        {
                            request.Sink.Request(request.Id);
                        }
                        request.TryFlushCache();
                    }
                }
            }
        }

    }
}
