﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders Raphael Boissel be liable for any claim,  *
 * damages or other liability, whether in an action of contract,  *
 * tort or otherwise, arising from, out of or in connection with  *
 * the software or the use or other dealings in the Software.     *
 *                                                                *
 * Except as contained in this notice, the name of                *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from Raphael Boissel.      *
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Locks
{
    /// <summary>
    /// ReadWrite Lock
    /// </summary>
    public class RWLock
    {
        volatile int rcount = 0;
        volatile int wcount = 0;
        volatile int wcountr = 0;
        /// <summary>
        /// Acquire the read lock. Multiple threads can
        /// hold a read lock at the same time. Read lock and
        /// Write lock are mutually exclusive.
        /// </summary>
        public void ReadLock()
        {
            lock (this)
            {

                if (wcount > 0 || wcountr > 0)
                {
                    while (wcount != 0 || wcountr > 0) { System.Threading.Monitor.Pulse(this); System.Threading.Monitor.Wait(this); }
                }
                rcount++;
            }
        }
        public void ReadUnlock()
        {
            lock (this)
            {
                rcount--;
                if (rcount < 0) { rcount = 0; }
                System.Threading.Monitor.Pulse(this);
            }
        }
        /// <summary>
        /// Acquire the write lock. Multiple threads can't
        /// hold a write lock at the same time. Read lock and
        /// Write lock are mutually exclusive.
        /// </summary>
        public void WriteLock()
        {
            lock (this)
            {
                wcountr++;
                while (wcount != 0 || rcount > 0) { System.Threading.Monitor.Pulse(this); System.Threading.Monitor.Wait(this); }
                wcountr--;
                if (wcountr == 0) { wcountr = 0; }
                wcount = 1;
            }
        }
        public void WriteUnlock()
        {
            lock (this)
            {
                wcount = 0;
                System.Threading.Monitor.Pulse(this);
            }
        }
    }
}
