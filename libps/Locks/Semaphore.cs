﻿/*
 * Copyright © 2013, Raphael Boissel                              *
 * Permission is hereby granted, free of charge, to any           *
 * person obtaining a copy of this software and associated        *
 * documentation files (the "Software"), to deal in the           *
 * Software without restriction, including without limitation     *
 * the rights to use, copy, modify, merge, publish, distribute,   *
 * sublicense, and/or sell copies of the Software, and to         *
 * permit persons to whom the Software is furnished to do         *
 * so, subject to the following conditions:                       *
 *                                                                *
 * The above copyright notice and this permission notice          *
 * shall be included in all copies or substantial portions        *
 * of the Software.                                               *
 *                                                                *
 * The Software is provided "as is", without warranty of any      *
 * kind, express or implied, including but not limited to the     *
 * warranties of merchantability, fitness for a particular        *
 * purpose and noninfringement. In no event shall the authors     *
 * or copyright holders Raphael Boissel be liable for any claim,  *
 * damages or other liability, whether in an action of contract,  *
 * tort or otherwise, arising from, out of or in connection with  *
 * the software or the use or other dealings in the Software.     *
 *                                                                *
 * Except as contained in this notice, the name of                *
 * Raphael Boissel shall not be used in advertising or otherwise  *
 * to promote the sale, use or other dealings in this Software    *
 * without prior written authorization from Raphael Boissel.      *
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace libps.Locks
{
    public class Semaphore
    {
        volatile int count = 1;
        volatile int max = 1;
        public int MaxAccess
        {
            get { lock (this) { return max; } }
            set { lock (this) { count += (value - max); max = value; if (max < 0) { max = 0; } if (count < 0) { count = 0; } } }
        }
        public void Lock()
        {
            lock (this)
            {
                while (count == 0) { System.Threading.Monitor.Wait(this); }
                count--;
                if (count < 0) { count = 0; }
            }
        }
        public void Unlock()
        {
            lock (this)
            {
                count++;
                if (count > max) { count = max; }
                System.Threading.Monitor.Pulse(this);
            }
        }
    }
}
